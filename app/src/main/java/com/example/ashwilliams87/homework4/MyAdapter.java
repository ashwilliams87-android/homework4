package com.example.ashwilliams87.homework4;

import android.media.MediaPlayer;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.FighterImageViewHolder> {
    private ArrayList<Fighter> fightersList;
    private final View.OnClickListener mOnClickListener = new MyOnClickListener();

    // Provide a suitable constructor (depends on the kind of dataset)
    MyAdapter(ArrayList<Fighter> fightersList) {
        this.fightersList = fightersList;
    }

    @NonNull
    @Override
    public FighterImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.fighter_recyler_view, parent, false);
        itemView.setOnClickListener(mOnClickListener);
        return new FighterImageViewHolder(itemView);
    }

    // Replace the contents of a view (invoked by the layout manager)
    public void onBindViewHolder(@NonNull FighterImageViewHolder holder, int position) {
        Fighter fighter = fightersList.get(position);
        Picasso.get().load(fighter.getImageUrl()).into(holder.fighterImage);
        holder.fighterName.setText(fighter.getName());
    }

    @Override
    public int getItemCount() {
        return fightersList.size();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    static class FighterImageViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        ImageView fighterImage;
        TextView fighterName;

        FighterImageViewHolder(View itemView) {
            super(itemView);
            fighterImage = itemView.findViewById(R.id.fighter_photo);
            fighterName = itemView.findViewById(R.id.fighter_name);
        }
    }

    private class MyOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(final View view) {
            Toast.makeText(view.getContext(), "Get over here!", Toast.LENGTH_LONG).show();
            MediaPlayer mPlayer = MediaPlayer.create(view.getContext(), R.raw.mk);
            mPlayer.start();
        }
    }
}
