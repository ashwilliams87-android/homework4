package com.example.ashwilliams87.homework4;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private RecyclerView myRecylerView;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myRecylerView = (RecyclerView) findViewById(R.id.my_recycler_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        myRecylerView.setHasFixedSize(true);
        // use a linear layout manager
        myRecylerView.setLayoutManager(new LinearLayoutManager(this));
        ArrayList<Fighter> fighterList = new ArrayList<>();
        fighterList.add(new Fighter("Fighter1", "http://mortalkombatframedata.com/application/images/fighter1.png"));
        fighterList.add(new Fighter("Fighter2", "http://mortalkombatframedata.com/application/images/fighter2.png"));
        fighterList.add(new Fighter("Fighter3", "http://mortalkombatframedata.com/application/images/fighter3.png"));
        fighterList.add(new Fighter("Fighter4", "http://mortalkombatframedata.com/application/images/fighter4.png"));
        fighterList.add(new Fighter("Fighter5", "http://mortalkombatframedata.com/application/images/fighter5.png"));
        fighterList.add(new Fighter("Fighter6", "http://mortalkombatframedata.com/application/images/fighter6.png"));
        fighterList.add(new Fighter("Fighter7", "http://mortalkombatframedata.com/application/images/fighter7.png"));
        fighterList.add(new Fighter("Fighter8", "http://mortalkombatframedata.com/application/images/fighter8.png"));
        fighterList.add(new Fighter("Fighter9", "http://mortalkombatframedata.com/application/images/fighter9.png"));
        fighterList.add(new Fighter("Fighter10", "http://mortalkombatframedata.com/application/images/fighter10.png"));
        fighterList.add(new Fighter("Fighter11", "http://mortalkombatframedata.com/application/images/fighter11.png"));
        fighterList.add(new Fighter("Fighter12", "http://mortalkombatframedata.com/application/images/fighter12.png"));
        RecyclerView.Adapter adapter = new MyAdapter(fighterList);
        myRecylerView.setAdapter(adapter);
    }
}
