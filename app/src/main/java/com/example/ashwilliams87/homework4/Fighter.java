package com.example.ashwilliams87.homework4;

public class Fighter {
    private String name;
    private String imageUrl;

    Fighter(String name, String imageUrl) {
        this.name = name;
        this.imageUrl = imageUrl;
    }

    public String getImageUrl() {
        return this.imageUrl;
    }

    public String getName() {
        return this.name;
    }
}
